#!/usr/bin/python
from gevent.pywsgi import WSGIServer


def application(env, start_response):
    start_response('301 OK', [
        ('Content-Type', 'text/html'),
        ("Location", "file:///etc/passwd"),
    ])
    return ["This is a redirect"]


if __name__ == '__main__':
    print 'Serving on 8088...'
    WSGIServer(('127.0.0.1', 8088), application).serve_forever()