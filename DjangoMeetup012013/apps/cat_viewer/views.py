from urllib import urlopen
from base64 import b64encode

from django.views.generic import TemplateView
from django.shortcuts import redirect

from forms import CatViewForm
from models import CatView

class CatViewer(TemplateView):
    template_name = 'view_cats.html'

    def post(self, request, *args, **kwargs):
        form = CatViewForm(self.request.POST)
        if not form.is_valid():
            return self.get(request, form=form)
        form.instance.cat_image = b64encode(urlopen(form.instance.picture_url).read())
        form.save()
        return redirect('cat_viewer')


    def get_context_data(self, **kwargs):
        return {
            'form': kwargs['form'] if 'form' in kwargs else CatViewForm(),
            'cat_pics': CatView.objects.all(),
        }
