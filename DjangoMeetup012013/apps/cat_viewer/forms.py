from django import forms
from models import CatView

class CatViewForm(forms.ModelForm):
    class Meta:
        model = CatView
        exclude = ('cat_image',)
#
#    def clean_picture_url(self):
#        url = self.cleaned_data.get('picture_url')
#        if not url or not url.startswith("http"):
#            raise forms.ValidationError("Cat URL must start with http")
#        return url
