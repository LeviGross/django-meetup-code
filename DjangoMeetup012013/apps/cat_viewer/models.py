from django.db import models

class CatView(models.Model):
    cat_name = models.CharField(max_length=100)
    picture_url = models.CharField(max_length=200)
    cat_image = models.TextField()
