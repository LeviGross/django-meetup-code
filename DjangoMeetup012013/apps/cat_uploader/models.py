from django.db import models

class CatPics(models.Model):
    name = models.CharField(max_length=100)
    pic = models.FileField(upload_to='catpics')

