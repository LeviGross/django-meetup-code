from django.views.generic import TemplateView
from django.shortcuts import redirect

from forms import CatUploaderForm
from models import CatPics

class UploadCats(TemplateView):
    template_name = 'upload_cats.html'

    def post(self, request, *args, **kwargs):
        form = CatUploaderForm(self.request.POST, self.request.FILES)
        if not form.is_valid():
            return self.get(request, form=form)
        form.save()
        return redirect('cat_uploader')


    def get_context_data(self, **kwargs):
        return {
            'form': kwargs['form'] if 'form' in kwargs else CatUploaderForm(),
            'cat_pics': CatPics.objects.all(),
            }
