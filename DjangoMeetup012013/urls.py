from django.contrib import admin
from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.conf import settings
from django.conf.urls.static import static

from apps.cat_viewer.views import CatViewer
from apps.cat_uploader.views import UploadCats


# See: https://docs.djangoproject.com/en/dev/ref/contrib/admin/#hooking-adminsite-instances-into-your-urlconf
admin.autodiscover()


# See: https://docs.djangoproject.com/en/dev/topics/http/urls/
urlpatterns = patterns('',
    # Admin panel and documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', TemplateView.as_view(template_name='home.html'), name='home_page'),
    url('^cat_viewer/$', CatViewer.as_view(), name="cat_viewer"),
    url('^cat_uploader/$', UploadCats.as_view(), name="cat_uploader"),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
